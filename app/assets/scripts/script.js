window.addEventListener('scroll', function () {
  var mainHeader = document.getElementsByClassName('main-header')[0];
  var mainNav = document.getElementsByClassName('main-header__navigation')[0];
  var topOffset = window.pageYOffset;
  
  if (topOffset >= (mainHeader.offsetHeight - mainNav.offsetHeight)) {
    mainNav.classList.add('main-header__navigation_fixed');
  } else {
    mainNav.classList.remove('main-header__navigation_fixed');
  }
});
